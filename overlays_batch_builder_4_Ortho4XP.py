"""
Overlays_batch_builder_4_Ortho4XP.py

A script for overlays batch building
by Nicola Marangon (aka nicotum, aka nicotum-xp)
v1.00

Scope of this program is to permit overlays build process in batch mode.

Requirements:
Ortho4XP v1.20b installed and working

Installation:
Drop this script under the Ortho4XP folder

Running the script:
Run the python interpreter in the usual way:
python Ortho4XP_build_overlays.py 

Running the overlays build process:
- Specify the latitude and longitude ranges you're gonna try to build
- Specify the Custom overlays folder (the script will load wathever it will find inside the Ortho4XP.cfg file)
- Run the process

Notes:
- If a DSF is not present inside the Custom overlays folder it will be skipped.

Credits: 
The wonderful Ortho4XP by OscarPilote

"""
import threading
from math import floor
import os
import random
import sys

import tkinter as tki
from tkinter import *
from tkinter import messagebox
from tkinter.scrolledtext import ScrolledText

from Ortho4XP_v120b import build_overlay

__CAPTION__ = 'Overlays batch builder for Ortho4XP'
__VERSION__ = '1.00'


class IORedirector(object):
    def __init__(self, root, text_target):
        self.root = root
        self.text_target = text_target

    def flush(self):
        pass


class StdoutRedirector(IORedirector):
    def write(self, text):
        self.text_target.insert(END, text)
        self.text_target.see(END)
        self.root.update_idletasks()


class UI(Tk):

    def __init__(self, sniff_dir=''):

        Tk.__init__(self)
        self.title('{} v{} - by N.Marangon'.format(__CAPTION__, __VERSION__))

        self.lat_min = IntVar(self, value=0)
        self.lat_max = IntVar(self, value=0)
        self.lon_min = IntVar(self, value=0)
        self.lon_max = IntVar(self, value=0)

        self.lat_min = IntVar(self, value=60)
        self.lat_max = IntVar(self, value=60)
        self.lon_min = IntVar(self, value=-120)
        self.lon_max = IntVar(self, value=-115)

        self.sniff_dir = StringVar(self, value=sniff_dir)

        entry_kwargs = {
            'anchor': 'w',
            'padx': 5,
            'pady': 5,
        }

        label_kwargs = {
            'side': LEFT,
            'padx': 5,
            'pady': 5,
        }

        self.frameLatMin = Frame(self)
        self.frameLatMin.pack(fill=X)
        self.lblLatMin = Label(self.frameLatMin, anchor='e', text="Latitude Min", width=25)
        self.lblLatMin.pack(**label_kwargs)
        self.txtLatMin = Entry(self.frameLatMin, textvariable=self.lat_min, width=7)
        self.txtLatMin.pack(**entry_kwargs)

        self.frameLonMin = Frame(self)
        self.frameLonMin.pack(fill=X)
        self.lblLonMin = Label(self.frameLonMin, anchor='e', text="Longitude Min", width=25)
        self.lblLonMin.pack(**label_kwargs)
        self.txtLonMin = Entry(self.frameLonMin, textvariable=self.lon_min, width=7)
        self.txtLonMin.pack(**entry_kwargs)

        self.frameLatMax = Frame(self)
        self.frameLatMax.pack(fill=X)
        self.lblLatMax = Label(self.frameLatMax, anchor='e', text="Latitude Max", width=25)
        self.lblLatMax.pack(**label_kwargs)
        self.txtLatMax = Entry(self.frameLatMax, textvariable=self.lat_max, width=7)
        self.txtLatMax.pack(**entry_kwargs)

        self.frameLonMax = Frame(self)
        self.frameLonMax.pack(fill=X)
        self.lblLonMax = Label(self.frameLonMax, anchor='e', text="Longitude Max", width=25)
        self.lblLonMax.pack(**label_kwargs)
        self.txtLonMax = Entry(self.frameLonMax, textvariable=self.lon_max, width=7)
        self.txtLonMax.pack(**entry_kwargs)

        self.frameSniffDir = Frame(self)
        self.frameSniffDir.pack(fill=X)
        self.lblSniffDir = Label(self.frameSniffDir, anchor='e', text="Custom Overlay Directory", width=25)
        self.lblSniffDir.pack(**label_kwargs)
        self.sniff_dir_entry = Entry(self.frameSniffDir, width=80, textvariable=self.sniff_dir)
        self.sniff_dir_entry.pack(fill=X, **entry_kwargs)

        self.frameButtons = Frame(self)
        self.frameButtons.pack(fill=X)
        self.btnBuild = Button(self.frameButtons, text='Build Overlays', command=self.build_overlays_init)
        self.btnBuild.pack(side=RIGHT, padx=5, pady=5)
        self.btnClose = Button(self.frameButtons, text='Close', command=self.close)
        self.btnClose.pack(side=RIGHT, padx=5, pady=5)
        self.btnCredits = Button(self.frameButtons, text='Credits', command=self.credits)
        self.btnCredits.pack(side=LEFT, padx=5, pady=5)

        self.frameOutput = Frame(self)
        self.frameOutput.pack(fill=X)
        self.txtOutput = ScrolledText(self.frameOutput, width=130,)
        self.txtOutput.pack(side=BOTTOM, padx=5, pady=10, anchor='s')

        self.frameStop = Frame(self)
        self.frameStop.pack(fill=X)
        self.btnStop = Button(self.frameStop, text='Stop process', command=self.stop)
        self.btnStop.pack(side=RIGHT, padx=5, pady=5)

        sys.stdout = StdoutRedirector(self, self.txtOutput)

        self.stop_semaphore = BooleanVar()
        self.stop_semaphore.set(False)

    def stop(self):
        answer = messagebox.askquestion("Stop process", "Are You Sure?", icon='info')
        if answer == 'yes':
            self.stop_semaphore.set(True)

    @staticmethod
    def close():

        answer = messagebox.askquestion("Exit request", "Are You Sure?", icon='info')
        if answer == 'no':
            return False

        sys.exit()

    @staticmethod
    def credits():

        messagebox.showinfo(
            __CAPTION__,
            "Credits:\n\n- The wonderful Ortho4XP by OscarPilote", icon='info')

    def validate(self):

        try:
            lat_min = int(self.lat_min.get())
            lat_max = int(self.lat_max.get())
            lon_min = int(self.lon_min.get())
            lon_max = int(self.lon_max.get())
        except ValueError:
            return False, "At least one lat,lon value isn't an integer"
        except tki.TclError:
            return False, "At least one lat,lon value isn't an integer"
        except Exception as ex:
            return False, ex

        if lat_min > lat_max:
            return False, "Latitude Min > Latitude Max"
        elif lon_min > lon_max:
            return False, "Longitude Min > Latitude Max"
        elif lat_min < -90 or lat_min > 89:
            return False, "Latitude Min must be between -90 and 89"
        elif lat_max < -90 or lat_max > 89:
            return False, "Latitude Max must be between -90 and 89"
        elif lon_min < -180 or lon_min > 179:
            return False, "Longitude Min must be between -180 and 179"
        elif lon_max < -180 or lon_max > 179:
            return False, "Longitude Max must be between -180 and 179"
        elif not str(self.sniff_dir.get()):
            return False, "Please, specify a Custom Overlay Directory"
        else:
            return True, ""

    def build_overlays_init(self):
        is_valid, err_msg = self.validate()

        if not is_valid:
            messagebox.showwarning("Error", err_msg)
            return False

        lat_min = int(self.lat_min.get())
        lat_max = int(self.lat_max.get())
        lon_min = int(self.lon_min.get())
        lon_max = int(self.lon_max.get())

        answer = messagebox.askquestion(
            "Build confirmation request",
            "You are going to build from lat {} to {} and lon {} to {}\n\nAre You Sure?".format(
                lat_min, lat_max, lon_min, lon_max),
            icon='info')

        if answer == 'no':
            return False

        build_overlays_thread = threading.Thread(target=self.build_overlays, args=[lat_min, lat_max, lon_min, lon_max])
        build_overlays_thread.start()

    def build_overlays(self, lat_min, lat_max, lon_min, lon_max):

        print("*** Building overlays from lat {} to {} and lon {} to {}".format(lat_min, lat_max, lon_min, lon_max))

        tile_number = (lat_max + 1 - lat_min) * (lon_max + 1 - lon_min)
        tile_idx = 0
        for lat in range(lat_min, lat_max + 1):

            for lon in range(lon_min, lon_max + 1):

                if self.stop_semaphore.get():
                    self.stop_semaphore.set(False)
                    print('*** Process stopped by User!')
                    return

                tile_idx += 1

                strlat = '{:+.0f}'.format(lat).zfill(3)
                strlon = '{:+.0f}'.format(lon).zfill(4)
                strlatround = '{:+.0f}'.format(floor(lat / 10) * 10).zfill(3)
                strlonround = '{:+.0f}'.format(floor(lon / 10) * 10).zfill(4)
                base_sniff_dir = str(self.sniff_dir.get())
                file_to_sniff = os.path.join(
                    base_sniff_dir, "Earth nav data", strlatround + strlonround, strlat + strlon + '.dsf')

                print('*** Building tile {} of {}: {}'.format(tile_idx, tile_number, file_to_sniff))
                if os.path.isfile(file_to_sniff):
                    try:
                        build_overlay(lat, lon, file_to_sniff)
                    except Exception as ex:
                        # messagebox.showwarning("Error", e)
                        print(ex)
                else:
                    print('*** Tile is not present inside custom overlay directory: skipping')

        messagebox.showinfo(
            __CAPTION__,
            "Build process complete", icon='info')


if __name__ == '__main__':

    default_sniff_dir = ''

    app_dir = os.getcwd()

    if hasattr(sys, 'real_prefix'):
        # we are inside a virtualenv!
        # Let's test if we are also inside a windows environment in order to workaround a virtualenv+tkinter bug
        if sys.platform == 'win32':
            os.environ["TCL_LIBRARY"] = app_dir + '\\bin\\tcl'
            os.environ["TK_LIBRARY"] = app_dir + '\\bin\\tk'

    try:
        exec(open(os.path.join(app_dir, 'Ortho4XP.cfg')).read())
    except Exception as e:
        print(e)

    application = UI(sniff_dir=default_sniff_dir)
    application.mainloop()
    application.quit()

    sys.exit()

